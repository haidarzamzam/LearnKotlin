package com.example.haidar.learnkotlin.viewmodels

import android.databinding.ObservableField
import com.example.haidar.learnkotlin.models.MahasiswaModel

class ViewModelMahasiswa {
    var nama = ObservableField<String>()
    var nim = ObservableField<String>()
    var semester = ObservableField<String>()

    var mhs = MahasiswaModel("nama","nim",0)

    init {
        nama.set(mhs.nama)
        nim.set(mhs.nim)
        semester.set(mhs.semester.toString())
    }
}