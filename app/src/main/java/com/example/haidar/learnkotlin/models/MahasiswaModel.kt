package com.example.haidar.learnkotlin.models

data class MahasiswaModel (var nama:String, var nim:String, var semester:Int)