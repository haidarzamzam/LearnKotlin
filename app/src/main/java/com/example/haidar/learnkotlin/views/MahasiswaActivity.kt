package com.example.haidar.learnkotlin.views

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.haidar.learnkotlin.R
import com.example.haidar.learnkotlin.databinding.ActivityMahasiswaBinding
import com.example.haidar.learnkotlin.viewmodels.ViewModelMahasiswa

class MahasiswaActivity : AppCompatActivity() {

    private lateinit var mhsBinding :ActivityMahasiswaBinding
    private lateinit var vmMahasiswa: ViewModelMahasiswa

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mhsBinding= DataBindingUtil.setContentView(this@MahasiswaActivity,R.layout.activity_mahasiswa)
        vmMahasiswa = ViewModelMahasiswa()
        mhsBinding.mahasiswa=vmMahasiswa
    }
}
